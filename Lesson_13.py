sqr1 = list(range(1, 101))
sqr2 = [n ** 2 for n in sqr1]
sumsqr = sum(sqr2)

sqr3 = list(range(1, 101))
sqrsum = sum(sqr3)
sqrsum *= sqrsum

diff = sqrsum - sumsqr
print(diff)