from typing import Counter


letters = input('Введите любые буквы :') + " " 
counter = 1

result = ""
for i in range(len(letters) -1) :
    if letters[i] == letters[i + 1] :
        counter += 1
    else:
        result += letters[i] + str(counter )
        counter = 1
print(result)
